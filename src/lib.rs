#![warn(
    missing_debug_implementations,
    missing_copy_implementations,
    rust_2018_idioms,
)]

///! # Barter-Integration

use crate::socket::{
    error::SocketError,
    protocol::websocket::WsMessage,
};
use std::{
    collections::HashMap,
    fmt::{Debug, Display, Formatter},
};
use serde::{Deserialize, Deserializer, Serialize};

/// Contains an `ExchangeSocket` capable of acting as a `Stream` and `Sink` for a given remote
/// server.
pub mod socket;

/// Barter representation of an `Instrument`. Used to uniquely identify a `base_quote` pair, and it's
/// associated instrument type.
///
/// eg/ Instrument { base: "btc", quote: "usdt", kind: Spot }
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug, Deserialize, Serialize)]
pub struct Instrument {
    pub base: Symbol,
    pub quote: Symbol,
    #[serde(rename = "instrument_type")]
    pub kind: InstrumentKind,
}

impl Display for Instrument {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}_{}, {}", self.base, self.quote, self.kind)
    }
}

impl<S> From<(S, S, InstrumentKind)> for Instrument
where
    S: Into<Symbol>,
{
    fn from((base, quote, kind): (S, S, InstrumentKind)) -> Self {
        Self {
            base: base.into(),
            quote: quote.into(),
            kind
        }
    }
}

impl Instrument {
    /// Constructs a new [`Instrument`] using the provided configuration.
    pub fn new<S>(base: S, quote: S, kind: InstrumentKind) -> Self
    where
        S: Into<Symbol>
    {
        Self {
            base: base.into(),
            quote: quote.into(),
            kind,
        }
    }

    /// Generates a unique identifier for an [`Instrument`] being traded on the provided exchange.
    pub fn to_id(&self, exchange: &str) -> InstrumentId {
        InstrumentId::new(self, exchange)
    }
}

/// Defines the type of [`Instrument`] which is being traded on a given `base_quote` market.
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum InstrumentKind {
    Spot,
    FuturePerpetual,
}

impl Default for InstrumentKind {
    fn default() -> Self {
        Self::Spot
    }
}

impl Display for InstrumentKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            InstrumentKind::Spot => "spot",
            InstrumentKind::FuturePerpetual => "future_perpetual",
        })
    }
}

/// Barter new type representing a currency symbol `String` identifier.
///
/// eg/ "btc", "eth", "usdt", etc
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize)]
pub struct Symbol(String);

impl Debug for Symbol {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Display for Symbol {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl AsRef<str> for Symbol {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl<'de> Deserialize<'de> for Symbol {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        String::deserialize(deserializer).map(Symbol::new)
    }
}

impl<S> From<S> for Symbol
where
    S: Into<String>
{
    fn from(input: S) -> Self {
        Symbol::new(input)
    }
}

impl Symbol {
    /// Construct a new [`Symbol`] new type using the provided `Into<Symbol>` value.
    pub fn new<S>(input: S) -> Self where S: Into<String> {
        Self(input.into().to_lowercase())
    }
}

/// Barter new type representing a unique `String` identifier for an [`Instrument`] being traded
/// on the provided exchange.
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize)]
pub struct InstrumentId(String);

impl Debug for InstrumentId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Display for InstrumentId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl AsRef<str> for InstrumentId {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl<'de> Deserialize<'de> for InstrumentId {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        String::deserialize(deserializer).map(InstrumentId)
    }
}

impl InstrumentId {
    /// Construct a unique `String` identifier for an [`Instrument`].
    pub fn new(instrument: &Instrument, exchange: &str) -> Self {
        Self(format!("{}_{}_{}_{}", exchange, instrument.base, instrument.quote, instrument.kind).to_lowercase())
    }
}

/// Barter [`Subscription`] used to subscribe to a market [`StreamKind`] for a particular
/// [`Instrument`].
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Deserialize, Serialize)]
pub struct Subscription {
    #[serde(flatten)]
    pub instrument: Instrument,
    #[serde(alias = "type")]
    pub kind: StreamKind,
}

impl Debug for Subscription {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}{}", self.kind, self.instrument)
    }
}

impl Display for Subscription {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl<I> From<(I, StreamKind)> for Subscription
where
    I: Into<Instrument>
{
    fn from((instrument, kind): (I, StreamKind)) -> Self {
        Self {
            instrument: instrument.into(),
            kind
        }
    }
}

impl<S> From<(S, S, InstrumentKind, StreamKind)> for Subscription
where
    S: Into<Symbol>
{
    fn from((base, quote, instrument, stream): (S, S, InstrumentKind, StreamKind)) -> Self {
        Self {
            instrument: Instrument::from((base, quote, instrument)),
            kind: stream
        }
    }
}

impl Subscription {
    /// Constructs a new [`Subscription`] using the provided configuration.
    pub fn new<I>(instrument: I, kind: StreamKind) -> Self
    where
        I: Into<Instrument>
    {
        Self {
            instrument: instrument.into(),
            kind
        }
    }
}

/// Possible Barter-Data Stream types a [`Subscription`] is associated with.
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum StreamKind {
    Trade,
    Candle(Interval),
    Kline(Interval),
    OrderBookDelta,
    OrderBook,
}

impl Display for StreamKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            StreamKind::Trade => "trade".to_owned(),
            StreamKind::Candle(interval) => format!("candle_{}", interval),
            StreamKind::Kline(interval) => format!("kline_{}", interval),
            StreamKind::OrderBookDelta => "order_book_delta".to_owned(),
            StreamKind::OrderBook => "order_book".to_owned()
        })
    }
}

/// Barter new type representing a time interval `String` identifier.
///
/// eg/ "1m", "1h", "12h", "1d", "1w", "1M", etc
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize)]
pub struct Interval(pub String);

impl Debug for Interval {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Display for Interval {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl AsRef<str> for Interval {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl<'de> Deserialize<'de> for Interval {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        String::deserialize(deserializer).map(Interval::new)
    }
}

impl<S> From<S> for Interval
where
    S: Into<String>
{
    fn from(input: S) -> Self {
        Self(input.into())
    }
}

impl Interval {
    /// Construct an [`Interval`] new type using the provided `Into<Interval>` value.
    pub fn new<S>(input: S) -> Self
        where
            S: Into<Interval>
    {
        input.into()
    }
}

#[derive(Clone, PartialEq, Debug)]
/// Metadata generated from a collection of Barter [`Subscription`]s. This includes the exchange
/// specific subscription payloads that are sent to the exchange.
pub struct SubscriptionMeta {
    /// `HashMap` containing the mapping between an incoming exchange message's [`SubscriptionId`],
    /// and a Barter [`Subscription`]. Used to identify the original [`Subscription`] associated
    /// with a received message.
    pub ids: SubscriptionIds,
    /// Number of [`Subscription`] responses expected from the exchange. Used to validate all
    /// [`Subscription`] were accepted.
    pub expected_responses: usize,
    /// Collection of [`WsMessage`]s containing exchange specific subscription payloads to be sent.
    pub subscriptions: Vec<WsMessage>,
}

/// Convenient type alias for a `HashMap` containing the mapping between an incoming exchange
/// message's [`SubscriptionId`], and a Barter [`Subscription`]. Used to identify the original
/// [`Subscription`] associated with a received message.
#[derive(Clone, Eq, PartialEq, Debug, Deserialize, Serialize)]
pub struct SubscriptionIds(pub HashMap<SubscriptionId, Subscription>);

impl SubscriptionIds {
    /// Find the [`Instrument`] associated with the provided `Into<SubscriptionId>`.
    pub fn find_instrument<Id>(&self, id: Id) -> Result<Instrument, SocketError>
    where
        Id: Into<SubscriptionId>
    {
        let subscription_id: SubscriptionId = id.into();
        self.0
            .get(&subscription_id)
            .map(|subscription| subscription.instrument.clone())
            .ok_or(SocketError::Unidentifiable(subscription_id))
    }
}

/// New type representing a unique `String` identifier for a stream that has been subscribed to.
/// This identifier is used to associated a [`Subscription`] with data structures received from
/// the exchange.
///
/// Note: Each exchange will require the use of different `String` identifiers depending on the
/// data structures they send.
///
/// eg/ [`SubscriptionId`] of an `FtxTrade` is "{BASE}/{QUOTE}" (ie/ market).
/// eg/ [`SubscriptionId`] of a `BinanceTrade` is "{base}{symbol}@trade" (ie/ channel).
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize)]
pub struct SubscriptionId(pub String);

impl Debug for SubscriptionId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Display for SubscriptionId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl AsRef<str> for SubscriptionId {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl<'de> Deserialize<'de> for SubscriptionId {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        String::deserialize(deserializer).map(SubscriptionId)
    }
}

impl<S> From<S> for SubscriptionId
where
    S: Into<String>,
{
    fn from(input: S) -> Self {
        Self(input.into())
    }
}

#[cfg(test)]
mod tests {
    use serde::de::Error;
    use super::*;
    use serde_json::Error as SerdeError;

    #[test]
    fn test_deserialise_subscription() {
        struct TestCase {
            input: &'static str,
            expected: Result<Subscription, SerdeError>,
        }
        
        let cases = vec![
            TestCase { // Test case 0: valid Spot Trade
                input: r#"{
                            "kind": "trade",
                            "base": "btc",
                            "quote": "usdt",
                            "instrument_type": "future_perpetual"
                        }"#,
                expected: Ok(Subscription {
                    instrument: ("btc", "usdt", InstrumentKind::FuturePerpetual).into(),
                    kind: StreamKind::Trade
                })
            },
            TestCase { // Test case 1: valid FuturePerpetual Trade w/ "type" as alias
                input: r#"{
                            "type": "trade",
                            "base": "btc",
                            "quote": "usdt",
                            "instrument_type": "spot"
                        }"#,
                expected: Ok(Subscription {
                    instrument: ("btc", "usdt", InstrumentKind::Spot).into(),
                    kind: StreamKind::Trade
                })
            },
            TestCase { // Test case 2: invalid Trade w/ invalid instrument_type
                input: r#"{
                            "type": "trade",
                            "b": "btc",
                            "q": "usdt",
                            "instrument_type": "unknown"
                        }"#,
                expected: Err(serde_json::error::Error::custom(""))
            }
        ];

        for (index, test) in cases.into_iter().enumerate() {
            match serde_json::from_str::<Subscription>(test.input) {
                Ok(actual) => assert_eq!(actual, test.expected.unwrap(), "TestCase {} failed", index),
                Err(err) => assert!(test.expected.is_err(), "TestCase {} failed with err: {}", index, err)
            }
        }
    }
}